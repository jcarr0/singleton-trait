## [0.4.0] - 2022-05-07

### Added

* The new SingleThread trait now asserts that a single-thread holds all references to a given type.

## [0.3.0] - 2021-08-28

### Added

* Improved documentation and added some example programs to the repository
* The `read` method now allow easy conversion of an `&Erased<&mut T>` into an `Erased<&T>`
* The `into_shared` method now allows easy conversion of an `Erased<&mut T>` into an `Erased<&T>`

### Changed

* The `Erased<T>: Clone` impl requires `T: Copy` now. This fixes a possible,
albeit niche soundness issue since impure Clone side-effects could be lost.
Code can use the `map_borrow` function to unsafely assert that a clone is pure.

### Fixed

* Many trivial methods are now correctly `#[inline(always)]`

### [0.2.0] - 2021-07-29

* Significantly improved flexibility for Exists trait

### Fixed

* Fixed some methods not being correctly inlinable (

## [0.1.2] - 2021-07-17

### Fixed

Fixed unclear licensing for repo

## [0.1.1] - 2021-07-08

### Fixed

Fixed markdown issue in README

## [0.1.0] - 2021-07-08

### Added

Initial release, with the Singleton trait, Erased<T> and Exists<T>
