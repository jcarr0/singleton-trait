use singleton_trait::Erased;
struct Token;


//
// This is a small example meant to exercise the usage
// of Erased borrows, ensuring that cases get handled
// well and there are no major usability problems
//

struct Env<'a> {
    write: Erased<&'a mut Token>,
    other_data: &'a mut u32,
    more_data: &'static (),
}

struct ReadEnv<'a> {
    read: Erased<&'a Token>,
    other_data: &'a u32,
}

impl<'a> Env<'a> {
    pub fn reborrow(&mut self) -> Env {
        let Env {write, other_data, more_data} = self;
        Env {
            write: self.write.reborrow(),
            other_data,
            more_data,
        }
    }

    pub fn into_shared(self) -> ReadEnv<'a> {
        ReadEnv {
            read: self.write.into_shared(),
            other_data: self.other_data
        }
    }
}

fn use_env(_: Env) {}

fn main() {
    let mut token = Token;
    let mut value = 0;
    let mut env = Env {
        write: Erased::new(&mut token),
        other_data: &mut value,
        more_data: &()
    };

    use_env(env.reborrow());
    use_env(env.reborrow());
    use_env(env);
}
