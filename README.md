
[![Version 0.4.0][version-badge]][changelog] [![MIT License][license-badge]][license]

[changelog]: ./CHANGELOG.md
[license]: ./LICENSE
[version-badge]: https://img.shields.io/badge/version-0.4.0-blue.svg
[license-badge]: https://shields.io/badge/license-MIT-green.svg

This library provides simple unsafe traits for types with global uniqueness properties,
as well as the Erased class for proofs.
This crate does not provide implementation strategies,
but does provide several examples of valid implementers.

Primarily, it is intended to be used for ghost proofs, in which ownership
of a value of particular type is sufficient in order for other safety conditions.

The type `Erased<T>` and the trait `Exists<T>` are additionally provided as zero-sized
proofs of ownership of a type (including references) when existence is enough for safety.
